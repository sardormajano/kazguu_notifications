/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry, StyleSheet, Text, View,
  Image, TextInput, TouchableHighlight,
  Navigator, BackAndroid
} from 'react-native';

import RegisterScene from './src/RegisterScene.js';
import LoginScene from './src/LoginScene.js';
import MainScene from './src/MainScene.js';

export default class kazguuNotifications extends Component {
    constructor(props) {
        super(props);

        this.state = {
            errorText: '',
            loadingText: '',
            username: '',
            password: '',
        };
    }

    loginHandler(navigator) {
        this.setState({
            loadingText: 'Загрузка...',
            errorText: ''
        });

       const details = {
//            username: '880805350470',
//            password: '!Q@W3e4r',
              username: this.state.username,
              password: this.state.password,
            grant_type: 'password'
        };

        let formBody = [];

        for (var property in details) {
          var encodedKey = encodeURIComponent(property);
          var encodedValue = encodeURIComponent(details[property]);
          formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");

        fetch('http://api.kazguu.growit.kz/Token', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded'
              },
            body: formBody
        })
        .then((response) => response.json())
        .then((responseJSON) => {
            if(responseJSON.error) {
                this.setState({
                    errorText: 'Неверный логин или пароль',
                    loadingText: ''
                });
            }
            else {
               navigator.push({
                 title: 'MainScene'
               });
            }
        })
        .catch((error) => {
            console.warn(error);
        });
    }

    toRegisterHandler(navigator) {
        navigator.push({
          title: 'RegisterScene'
        });
    }

    toLoginHandler(navigator) {
        navigator.push({
           title: 'LoginScene'
        });
    }

    render() {
    return (
      <Navigator
        initialRoute={{ title: 'LoginScene'}}
        renderScene={(route, navigator) => {
          BackAndroid.addEventListener('hardwareBackPress', (e) => {
            navigator.pop();
            return true;
          });

          switch(route.title) {
            case 'MainScene':
              return <MainScene context={this}/>;

            case 'LoginScene':
              return <LoginScene
                        context={this}
                        loginHandler={this.loginHandler.bind(this, navigator)}
                        toRegisterHandler={this.toRegisterHandler.bind(this, navigator)}
                     />;
            case 'RegisterScene':
              return <RegisterScene
                        signUpHandler={this.loginHandler.bind(this, navigator)}
                        toLoginHandler={this.toLoginHandler.bind(this, navigator)}
                     />;

            default:
              return <LoginScene
                         context={this}
                         loginHandler={this.loginHandler.bind(this, navigator)}
                         toRegisterHandler={this.toRegisterHandler.bind(this, navigator)}
                      />;
          }
        }}
      />
    );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('kazguuNotifications', () => kazguuNotifications);
