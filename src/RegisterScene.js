import React, { Component } from 'react';
import { Image, View } from 'react-native';
import {
  Container, Content, Footer, FooterTab, Header,
  Title, Button, Icon, Card, CardItem, InputGroup,
  Input, List, ListItem, Radio, Text, CheckBox,
  Body, Picker, Item
} from 'native-base';

import {styles} from './styles/styles.js';

export default class LoginScene extends Component {
  constructor(props) {
    super(props);

    this.state = {
        selected: 'key0'
    };
  }

  render() {
    return (
      <Container>
        <Header>
          <Title>Регистрация</Title>
        </Header>

        <Content
            style={
            {
                flex: 1,
                flexDirection: 'column'
            }
        }>
            <Card style={{marginTop: 20}}>
                <View style={{padding: 18}}>
                    <Picker
                        iosHeader="Select one"
                        mode="dropdown"
                        selectedValue={this.state.selected}
                        onValueChange={(value) => {this.setState({selected: value})}}
                        style={{paddingLeft: 1000}}
                    >
                        <Item label="Выберите группу" value="key0" disabled/>
                        <Item label="ЮРФ-216" value="key1" />
                        <Item label="ФИМ-316" value="key1" />
                        <Item label="БИЗ-116" value="key1" />
                        <Item label="ФФ-206" value="key1" />
                    </Picker>
                </View>
                <CardItem>
                    <InputGroup borderType='regular' >
                        <Input placeholder='Имя'/>
                    </InputGroup>
                </CardItem>
                <CardItem>
                    <InputGroup borderType='regular' >
                        <Input placeholder='Фамилия'/>
                    </InputGroup>
                </CardItem>
           </Card>
           <Card>
                <CardItem>
                    <InputGroup borderType='regular' >
                        <Input placeholder='Email'/>
                    </InputGroup>
                </CardItem>
                <CardItem>
                    <InputGroup borderType='regular' >
                        <Input
                            secureTextEntry={true}
                            placeholder='Придумайте пароль'/>
                    </InputGroup>
                </CardItem>
           </Card>
        </Content>

        <Footer>
          <FooterTab>
            <Button success onPress={this.props.loginHandler}>
                <Text style={{fontSize: 15, color: 'white'}}>Регистрация</Text>
            </Button>
          </FooterTab>
          <FooterTab>
            <Button light onPress={this.props.toLoginHandler}>
                <Text style={{fontSize: 15, color: 'black'}}>Есть акаунт</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
   )
  }
}
