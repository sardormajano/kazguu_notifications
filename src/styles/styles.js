import { StyleSheet, Dimensions } from 'react-native';

export const styles = {
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  }
};
