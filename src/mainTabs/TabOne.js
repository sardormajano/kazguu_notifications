import React, {Component} from 'react';
import {Image, View} from 'react-native';
import {
  Container, Content, Text,
  ListItem, Icon
} from 'native-base';

import {styles} from '../styles/styles.js';

import timer from 'react-native-timer';

const beautifyDate = (date) => {
    const dateString = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate(),
          monthString = date.getMonth() + 1 < 10 ? `0${date.getMonth() + 1}` : date.getMonth() + 1,
          yearString = date.getFullYear(),
          timeString = `${date.getHours()}:${date.getMinutes()}`;

    return `${dateString}.${monthString}.${yearString} - ${timeString}`;
}

const datesAreEqual = (d1, d2) => {
    return d1.getFullYear() === d2.getFullYear()
                && d1.getMonth() === d2.getMonth()
                && d1.getDate() === d2.getDate();
}

export default class TabOne extends Component {
  constructor(props) {
    super(props);

    this.state = {
        notifications: []
    };
  }

  componentDidMount() {
    timer.setInterval('main', () => {
        fetch(`http://api.kazguu.growit.kz/api/MobileApp/GetNotifications?username=${this.props.context.state.username}`, {
                method: 'GET'
            })
            .then((response) => response.json())
            .then((responseJSON) => {
                if(responseJSON.error) {
                    console.log('error');
                }
                else {
                   this.setState({notifications: responseJSON});
                }
            })
            .catch((error) => {
                console.warn(error);
            });
    }, 3000);
  }

  getNotificationsJSX(notificationsArray) {

    return notificationsArray.map((notification, index) => {
        let time;

        if(!index) {
            time = (
                <ListItem itemDivider>
                    <Text>{beautifyDate(new Date(notification.CreateDate))}</Text>
                </ListItem>
            );
        }
        else if(!datesAreEqual(new Date(notification.CreateDate), new Date(notificationsArray[index - 1].CreateDate))) {
            time = (
                <ListItem itemDivider>
                    <Text>{beautifyDate(new Date(notification.CreateDate))}</Text>
                </ListItem>
            );
        }
        else {
            time = [];
        }

        return (
            <View key={index}>
                {time}
                <ListItem>
                    <Icon style={{color: 'yellow', marginRight: 10}} name='warning' />
                    <Text style={{color: 'black'}}>{notification.Text}{'\n'} - {notification.From}</Text>
                </ListItem>
            </View>
        );
    });
  }

  render() {

    return (
      <Container>
        <Content>
            {this.getNotificationsJSX(this.state.notifications)}
        </Content>
      </Container>
    );
  }
}
