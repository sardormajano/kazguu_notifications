import React, {Component} from 'react';
import {Image} from 'react-native';
import {
  Container, Content, Text,
  ListItem, Icon, Badge
} from 'native-base';

import {styles} from '../styles/styles.js';

export default class TabOne extends Component {
  render() {
    return (
      <Container style={styles.container}>
          <Content>
              <ListItem itemHeader first style={{marginTop: 5}}>
                  <Text style={{fontWeight: 'bold'}}>Понедельник</Text>
              </ListItem>
              <ListItem style={{flex: 1, flexDirection: 'column'}}>
                  <Badge info style={{marginRight: 10}}>
                      <Text>11:00 - 11: 50</Text>
                  </Badge>
                  <Text>История Казахстана, 114 ауд., лабораторная</Text>
              </ListItem>
              <ListItem style={{flex: 1, flexDirection: 'column'}}>
                  <Badge info style={{marginRight: 10}}>
                      <Text>13:20 - 14: 05</Text>
                  </Badge>
                  <Text>Введение в микроэкономику, 254 ауд., лекция</Text>
              </ListItem>
              <ListItem style={{flex: 1, flexDirection: 'column'}}>
                  <Badge info style={{marginRight: 10}}>
                      <Text>16:00 - 17: 05</Text>
                  </Badge>
                  <Text>Алгоритм и Анализ, 322 ауд., лабораторная</Text>
              </ListItem>
              <ListItem itemHeader>
                  <Text style={{fontWeight: 'bold'}}>Вторник</Text>
              </ListItem>
              <ListItem style={{flex: 1, flexDirection: 'column'}}>
                  <Badge info style={{marginRight: 10}}>
                      <Text>11:00 - 11: 50</Text>
                  </Badge>
                  <Text>История Казахстана, 114 ауд., лабораторная</Text>
              </ListItem>
              <ListItem style={{flex: 1, flexDirection: 'column'}}>
                  <Badge info style={{marginRight: 10}}>
                      <Text>13:20 - 14: 05</Text>
                  </Badge>
                  <Text>Введение в микроэкономику, 254 ауд., лекция</Text>
              </ListItem>
              <ListItem style={{flex: 1, flexDirection: 'column'}}>
                  <Badge info style={{marginRight: 10}}>
                      <Text>16:00 - 17: 05</Text>
                  </Badge>
                  <Text>Алгоритм и Анализ, 322 ауд., лабораторная</Text>
              </ListItem>
              <ListItem itemHeader>
                  <Text style={{fontWeight: 'bold'}}>Среда</Text>
              </ListItem>
              <ListItem style={{flex: 1, flexDirection: 'column'}}>
                  <Badge info style={{marginRight: 10}}>
                      <Text>11:00 - 11: 50</Text>
                  </Badge>
                  <Text>История Казахстана, 114 ауд., лабораторная</Text>
              </ListItem>
              <ListItem style={{flex: 1, flexDirection: 'column'}}>
                  <Badge info style={{marginRight: 10}}>
                      <Text>13:20 - 14: 05</Text>
                  </Badge>
                  <Text>Введение в микроэкономику, 254 ауд., лекция</Text>
              </ListItem>
              <ListItem style={{flex: 1, flexDirection: 'column'}}>
                  <Badge info style={{marginRight: 10}}>
                      <Text>16:00 - 17: 05</Text>
                  </Badge>
                  <Text>Алгоритм и Анализ, 322 ауд., лабораторная</Text>
              </ListItem>
              <ListItem itemHeader>
                  <Text style={{fontWeight: 'bold'}}>Четверг</Text>
              </ListItem>
              <ListItem style={{flex: 1, flexDirection: 'column'}}>
                  <Badge info style={{marginRight: 10}}>
                      <Text>11:00 - 11: 50</Text>
                  </Badge>
                  <Text>История Казахстана, 114 ауд., лабораторная</Text>
              </ListItem>
              <ListItem style={{flex: 1, flexDirection: 'column'}}>
                  <Badge info style={{marginRight: 10}}>
                      <Text>13:20 - 14: 05</Text>
                  </Badge>
                  <Text>Введение в микроэкономику, 254 ауд., лекция</Text>
              </ListItem>
              <ListItem style={{flex: 1, flexDirection: 'column'}}>
                  <Badge info style={{marginRight: 10}}>
                      <Text>16:00 - 17: 05</Text>
                  </Badge>
                  <Text>Алгоритм и Анализ, 322 ауд., лабораторная</Text>
              </ListItem>
          </Content>
      </Container>
    );
  }
}
