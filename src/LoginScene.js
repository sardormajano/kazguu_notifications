import React, { Component } from 'react';
import { Image } from 'react-native';
import {
  Container, Content, Footer, FooterTab, Header,
  Title, Button, Icon, Card, CardItem, InputGroup,
  Input, Text
} from 'native-base';

import {styles} from './styles/styles.js'

export default class LoginScene extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {context} = this.props;

    return (
      <Container>
        <Header>
          <Title>Авторизация</Title>
        </Header>

        <Content>
            <Card style={{marginTop: 30}}>
                <CardItem style={{flex: 1, flexDirection: 'column', alignItems: 'center'}}>
                    <Image
                        source={require('./logo.png')} />
                </CardItem>
            </Card>
            <Card style={{marginTop: 30}}>
              <CardItem>
                <InputGroup borderType='regular' >
                    <Input
                        placeholder='Введите логин'
                        onChangeText={(username) => context.setState({username})}
                    />
                </InputGroup>
              </CardItem>
              <CardItem>
                <InputGroup borderType='regular' >
                    <Input
                        onChangeText={(password) => context.setState({password})}
                        secureTextEntry={true}
                        placeholder='Введите пароль'
                    />
                </InputGroup>
              </CardItem>
              <CardItem>
                <Text style={{color: 'red'}}>{context.state.errorText}</Text>
                <Text style={{color: 'grey'}}>{context.state.loadingText}</Text>
              </CardItem>
           </Card>
        </Content>

        <Footer>
          <FooterTab>
            <Button success onPress={this.props.loginHandler}>
                <Text style={{fontSize: 15, color: 'white'}}>Войти</Text>
            </Button>
          </FooterTab>
          <FooterTab>
            <Button light onPress={this.props.toRegisterHandler}>
                <Text style={{fontSize: 15, color: 'black'}}>Регистрация</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
   )
  }
}
