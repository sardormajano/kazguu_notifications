import React, { Component } from 'react';
import { Container, Content, Tab, Tabs, Header } from 'native-base';

import TabOne from './mainTabs/TabOne.js';
import TabTwo from './mainTabs/TabTwo.js';

export default class TabsExample extends Component {
    render() {
        return (
            <Container>
            <Header hasTabs />
            <Tabs>
                <Tab heading="Уведомления">
                    <TabOne context={this.props.context}/>
                </Tab>
                <Tab heading="Расписание">
                    <TabTwo context={this.props.context}/>
                </Tab>
            </Tabs>
            </Container>
        );
    }
}